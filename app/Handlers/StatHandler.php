<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 2/9/2018
 * Time: 5:11 PM
 */

namespace App\Handlers;


use App\Stat;
use BotMan\BotMan\BotMan;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

class StatHandler extends AbstractProcessingHandler
{
    public function isHandling(array $record)
    {
        return $record['level'] == Logger::INFO;
    }


    protected function write(array $record) {
        $context = $record['context'];
        if (!array_key_exists('type', $context) || !($context['type'] == 'stat')) {
            return;
        }

        /** @var BotMan $bot */
        $bot = $context['bot'];
        $userId = null;
        if ($bot->getUser()) {
            $userId = $bot->getUser()->getId();
        }
        $driver = $bot->getDriver()->getName();
        $recipientId = $bot->getMessage()->getRecipient();

        $stat = new Stat();
        $stat->level = $record['level_name'];
        $stat->message = $record['message'];
        $stat->date = $record['datetime'];
        $stat->method = $context['method'];
        $stat->driver = $driver;
        $stat->userId = $userId;
        $stat->recipientId = $recipientId;
        $stat->save();
    }


}