<?php

/**
 * Created by PhpStorm.
 * User: ader
 * Date: 2/4/2018
 * Time: 6:28 PM
 */

namespace App\Strategies;

use App\Order;
use App\OrderItem;
use BotMan\Drivers\Facebook\Extensions\ButtonTemplate;
use BotMan\Drivers\Facebook\Extensions\ElementButton;
use BotMan\Drivers\Facebook\Extensions\Element;
use BotMan\Drivers\Facebook\Extensions\GenericTemplate;
use BotMan\Drivers\Facebook\Extensions\ListTemplate;
use BotMan\Drivers\Facebook\Extensions\ReceiptAddress;
use BotMan\Drivers\Facebook\Extensions\ReceiptElement;
use BotMan\Drivers\Facebook\Extensions\ReceiptSummary;
use BotMan\Drivers\Facebook\Extensions\ReceiptTemplate;

class Facebook extends Strategy
{

    public function menu() {
        $menu = ButtonTemplate::create('Оберіть питання')
            ->addButtons([
                ElementButton::create('Часті питання')->type('postback')->payload('Часті питання'),
                ElementButton::create('Каталог')->type('postback')->payload('Каталог'),
                ElementButton::create('Відслідкувати')->type('postback')->payload('Відслідкувати'),
            ]);
        $this->bot->reply($menu);
    }

    public function faq()
    {
        $menu = ButtonTemplate::create('Оберіть зі списку пункт, який Вас цікавить')
            ->addButtons([
                ElementButton::create('Що таке Plastimake')->type('postback')->payload('Що таке Plastimake'),
                ElementButton::create('Доставка та оплата')->type('postback')->payload('Доставка та оплата'),
                ElementButton::create('Скільки коштує')->type('postback')->payload('Скільки коштує'),
            ]);
        $this->bot->reply($menu);
    }

    public function about() {
//        $attachment = new Video('https://botman.beedevs.com/Plastimake.mp4');
//                    $message = OutgoingMessage::create('Plastimake це термопластик')
//                        ->withAttachment($attachment);
//                    $this->bot->reply($message);

        $this->bot->reply(GenericTemplate::create()
            ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
            ->addElement(
                Element::create('Plastimake вебсайт')
                    ->subtitle('Відвідайте сайт для отримання детальної інформації, і не забудьте подивитися відео:)')
                    ->image('https://botman.beedevs.com/images/about.jpg')
                    ->addButton(ElementButton::create('Відвідати')
                        ->url('http://plastimake-ua.com')))
        );
    }

    public function catalog()
    {
        $list = ListTemplate::create()
            ->useCompactView()
            ->addGlobalButton(ElementButton::create('Корзина')
                ->payload('Корзина')->type('postback'));
        foreach(OrderItem::$CATALOG as $item) {
            $element = Element::create($item['name'])
                ->subtitle($item['description'])
                ->image($item['url'])
                ->addButton(ElementButton::create($item['id'])
                    ->payload($item['id'])->type('postback'));

            $list->addElement($element);
        }
        $this->bot->reply($list);
    }

    public function products($text) {
        $template = GenericTemplate::create()
            ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE);

        $menu = OrderItem::getMenuById($text);
        foreach ($menu['items'] as $item) {
            $element = Element::create($item->name)
                ->subtitle($item->description)
                ->image($item->url)
                ->addButton(ElementButton::create('Додати ' . $item->price . 'грн')
                    ->payload($item->id)->type('postback'));
            $template->addElement($element);
        }
        $this->bot->reply($template);

//        $bot->ask($template, function(Answer $answer){
//            if ($answer->isInteractiveMessageReply()) {
//                $item = OrderItem::getItemById($answer->getText());
//                if ($item) {
//                    OrderItem::$CART[] = $item;
//                }
//                self::$bot->reply('getCallbackId:'.$answer->getCallbackId());
//                self::$bot->reply('getPayload:'.$answer->getMessage()->getPayload());
//                self::$bot->reply('getText:'.$answer->getText());
//                self::$bot->reply('getValue:'.$answer->getValue());
//            }
//        });
    }

    public function show($itemId) {
        $template = GenericTemplate::create()
            ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE);

        /** @var OrderItem $item */
        $item = OrderItem::getItemById($itemId);
        $element = Element::create($item->name)
            ->subtitle($item->description)
            ->image($item->url)
            ->addButton(ElementButton::create('Додати ' . $item->price . 'грн')
                ->payload($item->id)->type('postback'));
        $template->addElement($element);
        $this->bot->reply($template);
    }

    public function cart() {
        $items = Order::restoreCart($this->bot);
        if (count($items) > 10) {
            $this->createTextCart($items);
        } else {
            if ($items) {
                $template = GenericTemplate::create()
                    ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE);

                foreach ($items as $item) {
                    $element = Element::create($item['name'])
                        ->subtitle($item['description'])
                        ->image($item['url'])
                        ->addButton(ElementButton::create('Видалити з корзини')
                            ->payload('Видалити '.$item['id'])->type('postback'));
                    $template->addElement($element);
                }

                $this->bot->reply($template);

            }

            $this->bot->reply('Корзина порожня');
        }
    }

    public function receipt() {
        $order = Order::restore($this->bot);
        if ($order instanceof Order) {
            $order = $order->toArray();
        }
        $receipt = ReceiptTemplate::create()
            ->recipientName($order['firstName'].' '.$order['lastName'])
            ->merchantName('ФОП Тимченко О.В.')
            ->orderNumber($order['number'])
            ->timestamp($order['timestamp'])
            ->orderUrl('http://test.at')
            ->currency('UAH')
            ->paymentMethod('Оплата по доставці')
            ->addAddress(ReceiptAddress::create()
                ->street1($order['address'])
                ->city($order['city'])
                ->postalCode('00000')
                ->state('Ukraine')
                ->country('Ukraine')
            );

        $total = Order::total($order['items']);
        $summary = ReceiptSummary::create()
            ->subtotal($total);

        if ($order['deliveryCost']) {
            $summary->shippingCost($order['deliveryCost']);
            $total += $order['deliveryCost'];
        }
        $summary->totalCost($total);
        $receipt->addSummary($summary);

        foreach($order['items'] as $item) {

            if ($item instanceof OrderItem) {
                $item = $item->toArray();
            }

            $receipt->addElement(
                ReceiptElement::create($item['name'])
                    ->price($item['price'])
                    ->image($item['url']));
        }

        $this->bot->reply($receipt);
    }

}