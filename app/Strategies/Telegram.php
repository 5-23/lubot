<?php

/**
 * Created by PhpStorm.
 * User: ader
 * Date: 2/4/2018
 * Time: 6:52 PM
 */

namespace App\Strategies;

use App\Order;
use App\OrderItem;
use \BotMan\Drivers\Telegram\Extensions\Keyboard;
use \BotMan\Drivers\Telegram\Extensions\KeyboardButton;

class Telegram extends Strategy
{

    public function menu() {
        $menu = Keyboard::create()
            ->addRow(
                KeyboardButton::create('Часті питання')->callbackData('Часті питання'),
                KeyboardButton::create('Каталог')->callbackData('Каталог'),
                KeyboardButton::create('Відслідкувати')->callbackData('Відслідкувати')
            );
        $this->bot->reply('Оберіть питання', $menu->toArray());
    }

    public function faq() {
        $menu = Keyboard::create()
            ->addRow(
                KeyboardButton::create('Що таке Plastimake')->callbackData('Що таке Plastimake'),
                KeyboardButton::create('Доставка та оплата')->callbackData('Доставка та оплата'),
                KeyboardButton::create('Скільки коштує')->callbackData('Скільки коштує')
            );
        $this->bot->reply('Оберіть зі списку пункт, який Вас цікавить', $menu->toArray());
    }

    public function about()
    {
        $menu = Keyboard::create()
            ->addRow(
                KeyboardButton::create('Відвідати')
                    ->url('http://plastimake-ua.com')
            );

        $this->sendPhoto($menu->toArray(), 'https://botman.beedevs.com/images/about.jpg',
            'Відвідайте сайт для отримання детальної інформації, і не забудьте подивитися відео:)');
    }

    public function products($text)
    {
        $menu = OrderItem::getMenuById($text);

        foreach ($menu['items'] as $item) {

            $add = Keyboard::create()
                ->addRow(
                    KeyboardButton::create('Додати ' . $item->price . 'грн')
                        ->callbackData($item->id)
                );
            $this->sendPhoto($add->toArray(), $item->url, $item->name);
        }
    }

    public function show($itemId) {
        /** @var OrderItem $item */
        $item = OrderItem::getItemById($itemId);

        $list = Keyboard::create()->addRow(
            KeyboardButton::create('Додати ' . $item->price . 'грн')
                ->callbackData($item->id)
        );

        $this->sendPhoto($list->toArray(), $item->url, $item->name);
    }

    public function catalog()
    {
        foreach(OrderItem::$CATALOG as $item) {

            $add = Keyboard::create()
                ->addRow(
                    KeyboardButton::create($item['id'])->callbackData($item['id'])
                );

            $this->sendPhoto($add->toArray(), $item['url'], $item['name']);
        }

        $cart = Keyboard::create()
            ->addRow(
                KeyboardButton::create('Корзина')->callbackData('Корзина')
            );
        $this->bot->reply('Перейти в корзину', $cart->toArray());
    }

    public function cart()
    {
        $items = Order::restoreCart($this->bot);
        if ($items) {
            $this->createTextCart($items);
        } else {
            $this->bot->reply('Корзина порожня');
        }
    }

    public function receipt()
    {
        //$order = Order::restore($this->bot);
    }

    protected function sendPhoto(Array $markup, $photoUrl, $text) {
        $recipient = $this->bot->getMessage()->getRecipient() === '' ? $this->bot->getMessage()->getSender() : $this->bot->getMessage()->getRecipient();
        $this->bot->sendRequest('sendPhoto', array_merge(
            [
                'chat_id' => $recipient,
                'photo' => $photoUrl,
                'caption' => $text
            ],
            $markup
        ));
    }

}