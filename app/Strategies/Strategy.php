<?php

/**
 * Created by PhpStorm.
 * User: ader
 * Date: 2/4/2018
 * Time: 6:37 PM
 */

namespace App\Strategies;

use \BotMan\BotMan\BotMan;

abstract class Strategy
{
    protected $bot;

    public function __construct(BotMan $bot) {
        $this->bot = $bot;
    }

    /**
     * @return BotMan
     */
    public function getBot() {
        return $this->bot;
    }

    public abstract function menu();
    public abstract function faq();
    public abstract function about();
    public abstract function products($text);
    public abstract function catalog();
    public abstract function show($itemId);
    public abstract function cart();
    public abstract function receipt();

    protected function createTextCart(Array $items) {
        $text = 'Корзина'.chr(10);
        $total = 0;
        foreach($items as $item) {
            $text .= $item['name'].' -- '.$item['price'].' грн'.chr(10);
            $total += (int)$item['price'];
        }
        $text .= 'Загалом: ' . $total . ' грн';
        $this->bot->reply($text);
    }
}