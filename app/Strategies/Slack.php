<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 2/6/2018
 * Time: 11:19 AM
 */

namespace App\Strategies;

use App\Order;
use App\OrderItem;

class Slack extends Strategy
{
    public function menu()
    {
        $menu = [
            "fallback" => "На вашому девайсі не вдалося відобразити форматований контент",
            "callback_id" => "menu",
            "attachment_type" => "default",
            "actions" => [
                [
                    "name" => "Часті питання",
                    "text" => "Часті питання",
                    "type" => "button",
                    "value" => "Часті питання"
                ],
                [
                    "name" => "Каталог",
                    "text" => "Каталог",
                    "type" => "button",
                    "value" => "Каталог"
                ],
                [
                    "name" => "Відслідкувати",
                    "text" => "Відслідкувати",
                    "type" => "button",
                    "value" => "Відслідкувати"
                ]
            ]
        ];
        $this->bot->reply("Оберіть питання", ["attachments" => json_encode([$menu])]);
    }

    public function faq()
    {
        $menu = [
            "fallback" => "На вашому девайсі не вдалося відобразити форматований контент",
            "callback_id" => "faq",
            "attachment_type" => "default",
            "actions" => [
                [
                    "name" => "Що таке Plastimake",
                    "text" => "Що таке Plastimake",
                    "type" => "button",
                    "value" => "Що таке Plastimake"
                ],
                [
                    "name" => "Доставка та оплата",
                    "text" => "Доставка та оплата",
                    "type" => "button",
                    "value" => "Доставка та оплата"
                ],
                [
                    "name" => "Скільки коштує",
                    "text" => "Скільки коштує",
                    "type" => "button",
                    "value" => "Скільки коштує"
                ]
            ]
        ];
        $this->bot->reply("Оберіть зі списку пункт, який Вас цікавить", ["attachments" => json_encode([$menu])]);

    }

    public function about()
    {
        $menu = [
            "fallback" => "На вашому девайсі не вдалося відобразити форматований контент",
            "callback_id" => "about",
            "attachment_type" => "default",
            "image_url" => "https://botman.beedevs.com/images/about.jpg",
            "actions" => [
                [
                    "name" => "Відвідати",
                    "text" => "Відвідати",
                    "type" => "button",
                    "url" => "http://plastimake-ua.com/"
                ]
            ]
        ];
        $this->bot->reply("Відвідайте сайт для отримання детальної інформації, і не забудьте подивитися відео:)", ["attachments" => json_encode([$menu])]);
    }

    public function products($text)
    {
        $menu = OrderItem::getMenuById($text);
        $attachments = [];
        $cart = [
            "text" => "Перейти в корзину",
            "fallback" => "На вашому девайсі не вдалося відобразити форматований контент",
            "callback_id" => "cart",
            "attachment_type" => "default",
            "actions" => [
                [
                    "name" => "Корзина",
                    "text" => "Корзина",
                    "type" => "button",
                    "value" => "Корзина"
                ]
            ]
        ];

        foreach($menu['items'] as $item) {
            $element = [
                "title" => $item->name,
                "text" => $item->description,
                "callback_id" => $item->id,
                "thumb_url" => $item->url,
                "actions" => [
                    [
                        "name" => $item->id,
                        "text" => $item->id,
                        "type" => "button",
                        "value" => $item->id
                    ]
                ]
            ];
            $attachments[] = $element;
        }
        $attachments[] = $cart;

        $this->bot->reply('', ["attachments" => json_encode($attachments)]);
    }

    public function catalog()
    {
        $attachments = [];
        $cart = [
            "text" => "Перейти в корзину",
            "fallback" => "На вашому девайсі не вдалося відобразити форматований контент",
            "callback_id" => "catalog",
            "attachment_type" => "default",
            "actions" => [
                [
                    "name" => "Корзина",
                    "text" => "Корзина",
                    "type" => "button",
                    "value" => "Корзина"
                ]
            ]
        ];

        foreach(OrderItem::$CATALOG as $item) {
            $element = [
                "title" => $item['name'],
                "text" => $item['description'],
                "callback_id" => $item['id'],
                "thumb_url" => $item['url'],
                "actions" => [
                    [
                        "name" => $item['id'],
                        "text" => $item['id'],
                        "type" => "button",
                        "value" => $item['id']
                    ]
                ]
            ];
            $attachments[] = $element;
        }
        $attachments[] = $cart;

        $this->bot->reply('', ["attachments" => json_encode($attachments)]);
    }

    public function show($itemId)
    {
        /** @var OrderItem $item */
        $item = OrderItem::getItemById($itemId);
        $menu = [
            "title" => $item->name,
            "text" => $item->description,
            "fallback" => "На вашому девайсі не вдалося відобразити форматований контент",
            "callback_id" => "show",
            "attachment_type" => "default",
            "image_url" => $item->url,
            "actions" => [
                [
                    "name" => $item->id,
                    "text" => $item->id,//"Додати " . $item->price . "грн",
                    "type" => "button",
                    "value" => $item->id
                ]
            ]
        ];
        $this->bot->reply('', ["attachments" => json_encode([$menu])]);
    }

    public function cart()
    {
        $items = Order::restoreCart($this->bot);
        if ($items) {
            $this->createTextCart($items);
        } else {
            $this->bot->reply('Корзина порожня');
        }
    }

    public function receipt()
    {
        // TODO: Implement receipt() method.
    }


}