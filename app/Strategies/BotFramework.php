<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 2/14/2018
 * Time: 5:04 PM
 */

namespace App\Strategies;


use App\Order;
use App\OrderItem;

class BotFramework extends Strategy
{
    public function menu()
    {
        $menu = [
            "type" => "AdaptiveCard",
            "version" => "1.0",
            "actions" => [
                [
                    "type" => "Action.Submit",
                    "title" => "Часті питання",
                    "data" => "Часті питання"
                ],
                [
                    "type" => "Action.Submit",
                    "title" => "Каталог",
                    "data" => "Каталог"
                ],
                [
                    "type" => "Action.Submit",
                    "title" => "Відслідкувати",
                    "data" => "Відслідкувати"
                ]
            ]
        ];

        $this->bot->reply("Оберіть питання", ["attachments" => [
            [
                "contentType" => "application/vnd.microsoft.card.adaptive",
                "content" => $menu
            ]
        ]]);
    }

    public function faq()
    {
        $menu = [
            "type" => "AdaptiveCard",
            "version" => "1.0",
            "actions" => [
                [
                    "type" => "Action.Submit",
                    "title" => "Що таке Plastimake",
                    "data" => "Що таке Plastimake"
                ],
                [
                    "type" => "Action.Submit",
                    "title" => "Доставка та оплата",
                    "data" => "Доставка та оплата"
                ],
                [
                    "type" => "Action.Submit",
                    "title" => "Скільки коштує",
                    "data" => "Скільки коштує"
                ]
            ]
        ];

        $this->bot->reply("Оберіть зі списку пункт, який Вас цікавить", ["attachments" => [
            [
                "contentType" => "application/vnd.microsoft.card.adaptive",
                "content" => $menu
            ]
        ]]);
    }

    public function about()
    {
        $menu = [
            "title" => 'Plastimake вебсайт',
            "images" => [
                [
                    "alt" => "На фото іграшки зроблені з пластимейка",
                    "url" => "https://botman.beedevs.com/images/about.jpg"
                ]
            ],
            "buttons" => [
                [
                    "type" => "openUrl",
                    "title" => "Відвідати",
                    "value" => "http://plastimake-ua.com/"
                ]
            ]
        ];

        $this->bot->reply("Відвідайте сайт для отримання детальної інформації, і не забудьте подивитися відео:)", ["attachments" => [
            [
                "contentType" => "application/vnd.microsoft.card.hero",
                "content" => $menu
            ]
        ]]);
    }

    public function products($text)
    {
        $attachments = [];
        $menu = OrderItem::getMenuById($text);
        foreach ($menu['items'] as $item) {
            $attachment = [
                "contentType" => "application/vnd.microsoft.card.adaptive",
                "content" => [
                    "type" => "AdaptiveCard",
                    "version" => "1.0",
                    "body" => [
                        [
                            "type" => "Container",
                            "items" => [
                                [
                                    "type" => "TextBlock",
					                "text" => $item->name,
					                "weight" => "bolder",
                                    "size" => "medium"
				                ],
                                [
                                    "type" => "TextBlock",
					                "text" => $item->description,
					                "wrap" => true
                                ],
                                [
                                    "type" => "Image",
                                    "url" => $item->url,
                                    "size" => "large"
                                ]
                            ]
                        ]
                    ],
                    "actions" => [
                        [
                            "type" => "Action.Submit",
                            "title" => 'Додати ' . $item->price . 'грн',
                            "data" => $item->id
                        ]
                    ]
                ]
            ];
            $attachments[] = $attachment;
        }

        $this->bot->reply("", [
            "attachments" => $attachments,
            "attachmentLayout" => 'carousel'
        ]);
    }

    public function catalog()
    {
        $attachments = [];

        foreach(OrderItem::$CATALOG as $index=>$item) {

            $attachment = [
                "contentType" => "application/vnd.microsoft.card.adaptive",
                "content" => [
                    "type" => "AdaptiveCard",
                    "version" => "1.0",
                    "body" => [
                        [
                            "type" => "Container",
                            "items" => [
                                [
                                    "type" => "ColumnSet",
                                    "columns" => [
                                        [
                                            "type" => "Column",
                                            "width" => "stretch",
                                            "items" => [
                                                [
                                                    "type" => "TextBlock",
                                                    "text" => $item['name'],
                                                    "weight" => "bolder",
                                                    "wrap" => true
                                                ],
                                                [
                                                    "type" => "TextBlock",
                                                    "spacing" => "none",
                                                    "text" => $item['description'],
                                                    "isSubtle" => true,
                                                    "wrap" => true
                                                ]
                                            ]
                                        ],
                                        [
                                            "type" => "Column",
                                            "width" => "auto",
                                            "items" => [
                                                [
                                                    "type" => "Image",
                                                    "url" => $item['url'],
                                                    "size" => "large"
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    "actions" => [
                        [
                            "type" => "Action.Submit",
                            "title" => $item['id'],
                            "data" => $item['id']
                        ]
                    ]
                ]
            ];

            if ($index == count(OrderItem::$CATALOG)-1) {
                $attachment['content']['actions'][] = [
                    "type" => "Action.Submit",
                    "title" => "Перейти в корзину",
                    "data" => "Корзина"
                ];
            }
            $attachments[] = $attachment;
        }

        $this->bot->reply("", ["attachments" => $attachments]);

    }

    public function show($itemId)
    {
        /** @var OrderItem $item */
        $item = OrderItem::getItemById($itemId);
        $menu = [
            "type" => "AdaptiveCard",
            "version" => "1.0",
            "body" => [
                [
                    "type" => "Container",
                    "items" => [
                        [
                            "type" => "TextBlock",
					        "text" => $item->name,
					        "size" => "medium",
                            "weight" => "bolder"
				        ],
                        [
                            "type" => "TextBlock",
					        "text" => $item->description,
					        "wrap" => true
                        ],
                        [
                            "type" => "Image",
                            "url" => $item->url,
                            "size" => "large"
                        ]
                    ]
                ]
            ],
            "actions" => [
                [
                    "type" => "Action.Submit",
                    "title" => "Додати " . $item->price . "грн",
                    "data" => $item->id
                ]
            ]
        ];

        $this->bot->reply('', ["attachments" => [
            [
                "contentType" => "application/vnd.microsoft.card.adaptive",
                "content" => $menu
            ]
        ]]);
    }

    public function cart()
    {
        $items = Order::restoreCart($this->bot);
        if (count($items) > 10) {
            $this->createTextCart($items);
        } else {
            $attachments = [];
            foreach ($items as $item) {
                $attachment = [
                    "contentType" => "application/vnd.microsoft.card.adaptive",
                    "content" => [
                        "type" => "AdaptiveCard",
                        "version" => "1.0",
                        "body" => [
                            [
                                "type" => "Container",
                                "items" => [
                                    [
                                        "type" => "TextBlock",
                                        "text" => $item['name'],
                                        "weight" => "bolder",
                                        "size" => "medium"
                                    ],
                                    [
                                        "type" => "TextBlock",
                                        "text" => $item['description'],
                                        "wrap" => true
                                    ],
                                    [
                                        "type" => "Image",
                                        "url" => $item['url'],
                                        "size" => "large"
                                    ]
                                ]
                            ]
                        ],
                        "actions" => [
                            [
                                "type" => "Action.Submit",
                                "title" => 'Видалити ' . $item['price'] . 'грн',
                                "data" => 'Видалити ' . $item['id']
                            ]
                        ]
                    ]
                ];
                $attachments[] = $attachment;
            }
            $this->bot->reply("", ["attachments" => $attachments]);
        }
    }

    public function receipt()
    {
        $order = Order::restore($this->bot);
        if ($order instanceof Order) {
            $order = $order->toArray();
        }

        $items = [];
        foreach($order['items'] as $item) {
            if ($item instanceof OrderItem) {
                $item = $item->toArray();
            }
            $items[] = [
                "title" => $item['name'],
                "image" => [
                    "url" => $item['url']
                ],
                "quantity" => 1,
                "price" => $item['price']
            ];
        }

        $total = Order::total($order['items']);

        if ($order['deliveryCost']) {
            $total += $order['deliveryCost'];

            $items[] = [
                "title" => "Доставка",
                "quantity" => 1,
                "price" => $order['deliveryCost']
            ];
        }

        $receipt = [
            "title" => "Замовлення №" . $order['number'],
            "total" => $total,
            "items" => $items
        ];

        $this->bot->reply('', ["attachments" => [
            [
                "contentType" => "application/vnd.microsoft.com.card.receipt",
                "content" => $receipt
            ]
        ]]);
    }


}