<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 10/12/2017
 * Time: 1:57 AM
 */

namespace App;


class OrderItem implements \JsonSerializable
{

    public static $CATALOG;

    public function __construct($id, $name, $url, $price, $description)
    {
        $this->id = $id;
        $this->name = $name;
        $this->url = $url;
        $this->price = $price;
        $this->description = $description;
    }

    public $id;
    public $name;
    public $url;
    public $price;
    public $description;

    public static function initCatalog() {
        OrderItem::$CATALOG = [
            [
                'id' => 'Plastimake',
                'name' => 'Каталог Plastimake',
                'url' => 'https://botman.beedevs.com/images/catalog_jars.png',
                'description' => 'Термопластик для творчості',
                'items' => [
                    new OrderItem(
                        'p100',
                        'Plastimake 100г',
                        'https://botman.beedevs.com/images/bottle100.jpg',
                        199,
                        'Невеличка порція достатня для декількох виробів'),
                    new OrderItem(
                        'p200',
                        'Plastimake 200г',
                        'https://botman.beedevs.com/images/bottle200.jpg',
                        359,
                        'Порція для міні-проекта'),
                    new OrderItem(
                        'p300',
                        'Plastimake 300г',
                        'https://botman.beedevs.com/images/bottle300.jpg',
                        499,
                        'Порція для експериментаторів'),
                ]
            ],
            [
                'id' => 'Набори',
                'name' => 'Каталог наборів',
                'url' => 'https://botman.beedevs.com/images/catalog_bundles.png',
                'description' => 'Подарункові набори Plastimake',
                'items' => [
                    new OrderItem(
                        'b100',
                        'Набір "Мінімальний"',
                        'https://botman.beedevs.com/images/bundle_min.jpg',
                        399,
                        'Plastimake 100г; Термомиска 550мл; Барвники 3шт; Люмінесценти 2шт'),
                    new OrderItem(
                        'b200',
                        'Набір "Стандартний"',
                        'https://botman.beedevs.com/images/bundle_med.jpg',
                        699,
                        'Plastimake 200г; Термомиска 550мл; Барвники 5шт; Люмінесценти 5шт'),
                    new OrderItem(
                        'b300',
                        'Набір "Максимальний"',
                        'https://botman.beedevs.com/images/bundle_max.jpg',
                        999,
                        'Plastimake 300г; Термомиска 850мл; Барвники 8шт; Люмінесценти 8шт'),
                ]
            ],
            [
                'id' => 'Барвники',
                'name' => 'Каталог барвників',
                'url' => 'https://botman.beedevs.com/images/catalog_grans.jpg',
                'description' => 'Барвники Plastmiake',
                'items' => [
                    new OrderItem(
                        'black',
                        'Чорний барвник',
                        'https://botman.beedevs.com/images/gran-lilac.png',
                        25,
                        'Барвник для окрашування матеріалу'),
                    new OrderItem(
                        'red',
                        'Червоний барвник',
                        'https://botman.beedevs.com/images/gran-red.png',
                        25,
                        'Барвник для окрашування матеріалу'),
                    new OrderItem(
                        'orange',
                        'Оранжевий барвник',
                        'https://botman.beedevs.com/images/gran-orange.png',
                        25,
                        'Барвник для окрашування матеріалу'),
                    new OrderItem(
                        'yellow',
                        'Жовтий барвник',
                        'https://botman.beedevs.com/images/gran-yellow.png',
                        25,
                        'Барвник для окрашування матеріалу'),
                    new OrderItem(
                        'green',
                        'Зелений барвник',
                        'https://botman.beedevs.com/images/gran-green.png',
                        25,
                        'Барвник для окрашування матеріалу'),
                    new OrderItem(
                        'blue',
                        'Синій барвник',
                        'https://botman.beedevs.com/images/gran-blue.png',
                        25,
                        'Барвник для окрашування матеріалу'),
                    new OrderItem(
                        'lilac',
                        'Фіолетовий барвник',
                        'https://botman.beedevs.com/images/gran-lilac.png',
                        25,
                        'Барвник для окрашування матеріалу'),
                    new OrderItem(
                        'brown',
                        'Коричневий барвник',
                        'https://botman.beedevs.com/images/gran-lilac.png',
                        25,
                        'Барвник для окрашування матеріалу'),
                ]
            ],
            [
                'id' => 'Аксесуари',
                'name' => 'Каталог аксесуарів',
                'url' => 'https://botman.beedevs.com/images/catalog_tools.jpg',
                'description' => 'Темомиски та набори для ліплення',
                'items' => [
                    new OrderItem(
                        'plate550',
                        'Термомиска 550мл',
                        'https://botman.beedevs.com/images/termo_plate.jpg',
                        99,
                        'Миска для 100/200г пластика'),
                    new OrderItem(
                        'plate850',
                        'Термомиска 850мл',
                        'https://botman.beedevs.com/images/termo_plate.jpg',
                        129,
                        'Миска для 300г пластика'),
                    new OrderItem(
                        'tool1',
                        'Набір для ліплення',
                        'https://botman.beedevs.com/images/tool1.jpg',
                        99,
                        'Двосторонні стеки для ліплення 8шт'),
                ]
            ]
        ];
    }

    public static function getMenuById($id) {
        foreach(OrderItem::$CATALOG as $menu) {
            if ($menu['id'] == $id) {
                return $menu;
            }
        }
        return null;
    }

    public static function getItemById($id) {
        foreach(OrderItem::$CATALOG as $menu) {
            foreach($menu['items'] as $item) {
                if ($item->id == $id) {
                    return $item;
                }
            }
        }
        return null;
    }

    public static function getMenuByItemId($itemId) {
        foreach(OrderItem::$CATALOG as $menu) {
            foreach($menu['items'] as $item) {
                if ($item->id == $itemId) {
                    return $menu;
                }
            }
        }
        return null;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'url' => $this->url,
            'price' => $this->price,
            'description' => $this->description,
        ];
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }
}