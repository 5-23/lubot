<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stat extends Model
{
    public $table = 'stat';
    public $timestamps = false;
}
