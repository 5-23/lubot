<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;

class CartReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:cart:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command reminds customers about abandon cart';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $orders = [];
//
//        if ($storage = opendir(storage_path('botman'))) {
//            while (false !== ($entry = readdir($storage))) {
//                if ($entry != "." && $entry != "..") {
//                    $orders[] = $entry;
//                }
//            }
//
//            closedir($storage);
//        }
//
//        if ($orders) {
//            foreach($orders as $data) {
//                $order = file_get_contents($data);
//                if (false !== $order) {
//                    $order = json_decode($order, true);
//
//                    if (count($order['items']) > 0 && $order['status'] == Order::ACTIVE_STATUS) {
//                        $this->notify($order);
//                    }
//                }
//            }
//        }
        $this->info('test output');
    }

    protected function notify($order) {
        $driver = $order['driver'];
        $userId = $order['userId'];
        $userName = $order['firstName'];
        $email = $order['email'];

        if ($driver && $userId) {
            $this->call('notify:push', [
                'userId' => $userId,
                'driver' => $driver,
                'message' => 'Вітаю'.($userName ? ', '.$userName : '').'! Здається Ви залишили свою корзину із замовленням.
                 Я вирішив нагадати Вам, що для офорлення замовлення просто напишіть "Замовлення" в чаті та дайте відповідь
                 на кілька запитань по доставці. Для того, щоб продивитися корзину - напишіть "Корзина". Гарного Вам дня!'
            ]);
            $this->info('Push notification was sent to driver='.$driver.', userId='.$userId);
        } else if ($email) {
            $this->call('notify:email', [
                'email' => $email,
                'message' => ''
            ]);
            $this->error('Push notification was NOT sent to email='.$email);
        } else {
            $this->error('Push notification can\'t be sent');
        }
    }
}
