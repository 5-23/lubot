<?php

namespace App\Console\Commands;

use BotMan\BotMan\Http\Curl;
use BotMan\Drivers\Facebook\FacebookDriver;
use BotMan\Drivers\Slack\SlackDriver;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Console\Command;

class DeliveryUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:delivery:update {userId} {driver} {message}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sent PUSH notication about delivery status';

    /**
     * @var Curl
     */
    private $http;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Curl $http)
    {
        parent::__construct();
        $this->http = $http;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userId = $this->argument('userId');
        $message = $this->argument('message');
        $driver = $this->argument('driver');

        if ($driver == FacebookDriver::DRIVER_NAME) {
            $this->pushFacebook($userId, $message);
        } else if ($driver == SlackDriver::DRIVER_NAME) {
            $this->pushSlack($userId, $message);
        } else if ($driver == TelegramDriver::DRIVER_NAME) {
            $this->pushTelegram($userId, $message);
        } else {
            $this->error('Driver '.$driver.' is not registered');
        }
    }

    protected function pushFacebook($userId, $message) {
        $payload = [
            'recipient' => [
                'id' => $userId
            ],
            'message' => [
                'text' => $message,
            ],
            'tag' => 'SHIPPING_UPDATE'
        ];

        $response = $this->http->post(
            'https://graph.facebook.com/v2.6/me/messages?access_token='.config('botman.facebook.token'),
            [], $payload);

        $responseObject = json_decode($response->getContent());

        if ($response->getStatusCode() == 200) {
            $this->printSuccess($userId);
        } else if ($responseObject && $responseObject->error) {
            $this->printFaultWithMessage($userId, $responseObject->error->message);
        } else {
            $this->printFault($userId);
        }
    }

    protected function pushTelegram($userId, $message) {
        $payload = [
            'chat_id' => $userId,
            'text' => $message
        ];

        $response = $this->http->post(
            'https://api.telegram.org/bot'.config('botman.telegram.token').'/sendMessage',
            [], $payload);

        $responseObject = json_decode($response->getContent());

        if ($response->getStatusCode() == 200) {
            $this->printSuccess($userId);
        } else if ($responseObject && $responseObject->error_message) {
            $this->printFaultWithMessage($userId, $responseObject->error_message);
        } else {
            $this->printFault($userId);
        }
    }

    protected function pushSlack($userId, $message) {
        $payload = [
            'token' => config('botman.slack.token'),
            'channel' => $userId,
            'text' => $message,
            'as_user' => true
        ];

        $response = $this->http->post(
            'https://slack.com/api/chat.postMessage',
            [], $payload);

        $responseObject = json_decode($response->getContent());

        if ($response->getStatusCode() == 200) {
            $this->printSuccess($userId);
        } else if ($responseObject && $responseObject->error) {
            $this->printFaultWithMessage($userId, $responseObject->error->message);
        } else {
            $this->printFault($userId);
        }
    }

    private function printSuccess($userId) {
        $this->info('Delivery update was sent to userId='.$userId);
    }

    private function printFault($userId) {
        $this->error('Delivery update was NOT sent to userId='.$userId);
    }

    private function printFaultWithMessage($userId, $message) {
        $this->error('Delivery update was NOT sent to userId='.$userId.
            '. Error: '.$message);
    }
}
