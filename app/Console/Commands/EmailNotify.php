<?php

namespace App\Console\Commands;

use App\Mail\CartReminder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class EmailNotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:notify:email {email} {message}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify via email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $message = $this->argument('message');
        Mail::to($email)->send(new CartReminder());

    }
}
