<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 10/14/2017
 * Time: 3:01 AM
 */

namespace App\Conversations;


use App\Http\Controllers\CheckoutController;
use App\Order;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;

class CheckoutConversation extends Conversation
{

    public function run()
    {
        Order::set($this->bot, 'timestamp', time());

        $this->bot->reply('Для оформлення замовлення я маю запитати Ваші контактні дані..');
        $this->bot->ask('Напишіть Ваш телефон', function(Answer $answer) {
            $phone = $answer->getMessage()->getText();
            Order::set($this->bot, 'phone', $phone);

            $this->bot->ask('Ваша емейл адреса?', function(Answer $answer) {
                $email = $answer->getMessage()->getText();
                Order::set($this->bot, 'email', $email);

                $this->bot->ask('Напишіть з якого ви міста', function(Answer $answer) {
                    $city = trim($answer->getMessage()->getText());
                    Order::set($this->bot, 'city', $city);

//                     if (preg_match('/^(К|к)и(ї|е)в$/g', $city ) && Order::total(Order::restoreCart($this->bot)) >= Order::FREE_DELIVERY_SUM) {
//                         $question = Question::create('Чи бажаєте замовити доставку кур\'єром?')->addButtons([
//                             Button::create('Так (35₴)')->value('Так'),
//                             Button::create('Ні')->value('Ні'),
//                         ]);
//                         $this->bot->ask($question, function(Answer $answer) {
//                             if ($answer->isInteractiveMessageReply()) {
//                                 if ($answer->getMessage()->getText() == 'Так') {
//                                     Order::set($this->bot, 'deliveryCost', Order::DELIVERY_COST);
//                                 }
//                             }
//                         });
//                     }

                    $this->bot->ask('Яка адреса доставки?', function(Answer $answer) {
                        $address = $answer->getMessage()->getText();
                        Order::set($this->bot, 'address', $address);

                        $this->bot->ask('Чи бажаєте додати коментар до замовлення? Напишіть Так або Ні', [
                            [
                                'pattern' => 'Так|так|Да|да',
                                'callback' => function () {
                                    $this->bot->ask('Тепер додайте коментар до замовлення', function(Answer $answer) {
                                        $note = $answer->getMessage()->getText();
                                        Order::set($this->bot, 'note', $note);

                                        $this->bot->reply('Ваше замовлення в обробці. Очікуйте деякий час в чаті.');
                                        (new CheckoutController())->submit($this->bot);
                                    });
                                }
                            ],
                            [
                                'pattern' => 'Ні|ні|Нет|нет',
                                'callback' => function () {
                                    $this->bot->reply('Ваше замовлення в обробці. Очікуйте деякий час в чаті.');
                                    (new CheckoutController())->submit($this->bot);
                                }
                            ]
                        ]);
                    });
                });
            });
        });
    }

}