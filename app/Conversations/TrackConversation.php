<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 1/31/2018
 * Time: 5:51 PM
 */

namespace App\Conversations;


use App\Http\Controllers\TrackController;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;

class TrackConversation extends Conversation
{
    public function run()
    {
        $this->bot->ask('Для того щоб перевірити статус замовлення напишіть його номер (тільки 5 цифр)', function(Answer $answer) {
            $orderNumber = $answer->getMessage()->getText();
            (new TrackController())->status($this->bot, $orderNumber);
        });
    }

}