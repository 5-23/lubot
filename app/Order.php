<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 10/14/2017
 * Time: 12:37 PM
 */

namespace App;


use BotMan\BotMan\BotMan;

class Order implements \JsonSerializable
{
    const DONE_STATUS = 'done';
    const ERROR_STATUS = 'error';
    const ACTIVE_STATUS = 'active';

    public $number;
    public $email;
    public $phone;
    public $address;
    public $city;
    public $note;
    public $firstName;
    public $lastName;
    public $userId;
    public $recipientId;
    public $timestamp;
    public $deliveryCost = 0;
    public $items;
    public $status = self::ACTIVE_STATUS;
    public $driver;
    public $editedDate;

    const FREE_DELIVERY_SUM = 600;
    const DELIVERY_COST = 35;

    public static function total($items) {
        $total = 0;
        foreach($items as $item) {
            if ($item instanceof OrderItem) {
                $item = $item->toArray();
            }
            $total += $item['price'];
        }
        return $total;
    }

    public static function init(BotMan $bot) {
        $order = new Order();
        $order->firstName = $bot->getUser()->getFirstName();
        $order->lastName = $bot->getUser()->getLastName();
        $order->userId = $bot->getUser()->getId();
        $order->driver = $bot->getDriver()->getName();
        $order->items = [];
        self::save($bot, $order);
        return $order;
    }

    public static function save(BotMan $bot, $order) {
        if ($order instanceof Order) {
            $order = $order->toArray();
        }
        $order['editedDate'] = (new \DateTime())->format('d-M-Y H:i');
        $order['recipientId'] = $bot->getMessage()->getRecipient();
        $bot->userStorage()->delete();
        $bot->userStorage()->save([$order]);
    }

    public static function restore(BotMan $bot) {
        $orders = $bot->userStorage()->find();
        if ($orders && $orders->count() > 0) {
            $order = $orders->first();
        } else {
            $order = self::init($bot)->toArray();
        }
        return $order;
    }

    public static function remove(BotMan $bot) {
        $bot->userStorage()->delete();
    }

    public static function set(BotMan $bot, $field, $value) {
        $order = self::restore($bot);
        $order[$field] = $value;
        self::save($bot, $order);
    }

    public static function restoreCart(BotMan $bot) {
        $order = self::restore($bot);
        if (array_key_exists('items', $order)) {
            $items = $order['items'];
        } else {
            $items = [];
        }

        return $items;
    }

    public static function removeCart(BotMan $bot) {
        $order = self::restore($bot);
        $order['items'] = [];
        self::save($bot, $order);
    }

    public static function saveCart(BotMan $bot, Array $items) {
        $order = self::restore($bot);
        $order['items'] = $items;
        self::save($bot, $order);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'number' => $this->number,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'city' => $this->city,
            'note' => $this->note,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'userId' => $this->userId,
            'recipientId' => $this->recipientId,
            'timestamp' => $this->timestamp,
            'deliveryCost' => $this->deliveryCost,
            'items' => $this->items,
            'status' => $this->status,
            'driver' => $this->driver,
            'editedDate' => $this->editedDate
        ];
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

}