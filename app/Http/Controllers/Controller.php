<?php

namespace App\Http\Controllers;

use App\Strategies\Strategy;
use App\Strategies\Facebook;
use App\Strategies\Telegram;
use App\Strategies\Slack;
use BotMan\BotMan\BotMan;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Log;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /** @var  Strategy */
    private $strategy;

    private $strategies = [
        'Facebook',
        'Telegram',
        'Slack',
        'BotFramework'
    ];

    /**
     * @return Strategy
     */
    public function strategy(BotMan $bot)
    {
        return $this->initStrategy($bot);
    }

    public function stat($msg, $method, $bot)
    {
        Log::info($msg, [
            'type' => 'stat',
            'method' => $method,
            'bot' => $bot,
        ]);
    }

    protected function initStrategy(BotMan $bot) {
        $driveName = $bot->getDriver()->getName();
        if ($this->strategy && (new \ReflectionClass($this->strategy))->getShortName() == $driveName) {
            return $this->strategy;
        }

        foreach ($this->strategies as $strategy) {
            if ($driveName == $strategy) {
                $class = "App\\Strategies\\".$strategy;
                return $this->strategy = new $class($bot);
            }
        }
        return null;
    }
}
