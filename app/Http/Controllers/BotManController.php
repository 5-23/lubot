<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BotManController extends Controller
{
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');
        $botman->listen();
    }

    public function verify(Request $request) {
        if ($request->get('hub_mode') === 'subscribe' && $request->get('hub_verify_token') === env('FACEBOOK_VERIFICATION')) {
            return Response::create($request->get('hub_challenge'));
        }
        return Response::create('Failed validation. Make sure the validation tokens match.', 403);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tinker()
    {
        return view('tinker');
    }

}
