<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 1/31/2018
 * Time: 5:34 PM
 */

namespace App\Http\Controllers;


use App\Conversations\TrackConversation;
use BotMan\BotMan\BotMan;

class TrackController extends Controller
{
    public function track(BotMan $bot) {
        $bot->startConversation(new TrackConversation());

        $this->stat('Check order status', 'status', $bot);
    }

    public function status(BotMan $bot, $orderNumber) {
        $response = $this->send(['orderNumber' => $orderNumber]);
        if ($response) {
            $status = $response['status'];
            $bot->reply($status);
        } else {
            $bot->reply('Нажаль при відправці данних сталася помилка, повторіть спробу пізніше.');
        }
    }

    protected function send(Array $data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://plastimake-ua.com/track');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

        $json = json_decode($response, true);
        if ($json) {
            return $json;
        }

        return null;
    }

}