<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 5/24/2018
 * Time: 2:11 PM
 */

namespace App\Http\Controllers;


use BotMan\BotMan\BotMan;
use BotMan\BotMan\Http\Curl;
use BotMan\BotMan\Middleware\ApiAi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DialogFlowController
{
    public function handle(Request $request) {
        $dialogflow = ApiAi::create('b71dd842a2eb429d8a83793ad3d2fe6a')->listenForAction();

        $botman = resolve('botman');

        // Apply global "received" middleware
        $botman->middleware->received($dialogflow);

// Apply matching middleware per hears command
        $botman->hears('input.welcome', function (BotMan $bot) {
            // The incoming message matched the "my_api_action" on Dialogflow
            // Retrieve Dialogflow information:
            $extras = $bot->getMessage()->getExtras();
            $apiReply = $extras['apiReply'];
            $apiAction = $extras['apiAction'];
            $apiIntent = $extras['apiIntent'];

            Log::info('extras: '.json_encode($extras));
        })->middleware($dialogflow);

    }
}