<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 10/14/2017
 * Time: 3:00 AM
 */

namespace App\Http\Controllers;


use App\Conversations\CheckoutConversation;
use App\Order;
use BotMan\BotMan\BotMan;

class CheckoutController extends Controller
{

    public function cheque(BotMan $bot) {
        $bot->reply('Режим попереднього перегляду чека. Для оформлення замовлення напишіть "Оформити"');
        $this->strategy($bot)->receipt();

        $this->stat('Show receipt', 'cheque', $bot);
    }

    public function checkout(BotMan $bot) {
        $items = Order::restoreCart($bot);
        if ($items) {
            $bot->startConversation(new CheckoutConversation());
        } else {
            $bot->reply('Корзина порожня. Щоб додати товари до корзини перейдіть в розділ "Каталог"');
        }

        $this->stat('Start checkout process', 'checkout', $bot);
    }

    public function submit(BotMan $bot) {
        $order = Order::restore($bot);
        $data = $this->prepareData($order);
        $response = $this->send($data);
        if ($response) {
            $orderNumber = $response['orderNumber'];
            $order['number'] = $orderNumber;
            $order['status'] = Order::DONE_STATUS;
            Order::save($bot, $order);
            $bot->reply('Заявку прийнято! Наш менеджер зателефонує Вам найближчим часом, номер замовлення #'.$orderNumber);
            $this->strategy($bot)->receipt();
        } else {
            $order['status'] = Order::ERROR_STATUS;
            Order::save($bot, $order);
            $bot->reply('Нажаль при відправці данних сталася помилка, повторіть спробу, будь-ласка, написавши "оформити"');
        }
    }

    protected function send(Array $data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://plastimake-ua.com/submit');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

        $json = json_decode($response, true);
        if ($json) {
            return $json;
        }

        return null;
    }

    protected function prepareData($order) {
        $data = [
            'userInfo' => [
                'firstName' => $order['firstName'],
                'lastName' => $order['lastName'],
                'payload' => 'FacebookId: '.$order['userId'],
                'phone' => $order['phone'],
                'email' => $order['email'],
                'address' => $order['city'].', '.$order['address'],
                'referrer' => env('APP_NAME'),
                'description' => $order['note']
            ],
            'products' => []
        ];

        foreach($order['items'] as $item) {
            if (array_key_exists($item['id'], $data['products'] )) {
                $amount = $data['products'][$item['id']];
                $data['products'][$item['id']] = $amount + 1;
            } else {
                $data['products'][$item['id']] = 1;
            }
        }

        return [
            'order' => $data,
            'requestCallback' => 'false',
            'mobileOrder' => 'false'
        ];
    }

}