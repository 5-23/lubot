<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 10/13/2017
 * Time: 1:36 AM
 */

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use BotMan\BotMan\BotMan;

class CartController extends Controller
{

    public function cart(BotMan $bot) {
        $this->strategy($bot)->cart();
        $bot->reply('Щоб оформити замовлення напишіть "Оформити". Для того щоб очистити корзину - "Очистити"');

        $this->stat('Show cart status', 'cart', $bot);
    }

    public function removeFromCart(BotMan $bot) {
        $itemId = preg_split('/\s+/', $bot->getMessage()->getText())[1];
        $items = Order::restoreCart($bot);

        if ($items) {
            foreach($items as $index=>$item) {
                if ($item['id'] == $itemId) {
                    array_splice($items, $index, 1);
                    break;
                }
            }

            Order::saveCart($bot, $items);
            $bot->reply('Видалено. В корзині: '.count($items).' товарів на '.Order::total($items).'₴');
            $bot->reply('Примітка. Щоб подивитися оновлену корзину, напишіть "Корзина"');

            $this->stat('Remove product from cart, id='.$itemId, 'removeFromCart', $bot);
        }

    }

    public function addToCart(BotMan $bot) {
        $itemId = $bot->getMessage()->getText();
        $item = OrderItem::getItemById($itemId);
        if ($item) {
            $items = Order::restoreCart($bot);
            $items[] = $item;
            Order::saveCart($bot, $items);
            $bot->reply('Додано. В корзині: '.count($items).' товарів на '.Order::total($items).'₴');
        }

        $this->stat('Remove product from cart, id='.$itemId, 'addToCart', $bot);
    }

    public function clearCart(BotMan $bot) {
        Order::removeCart($bot);
        $bot->reply('Корзину очищено');

        $this->stat('Clear cart completely', 'clearCart', $bot);
    }

    public function clearOrder(BotMan $bot) {
        Order::remove($bot);
        $bot->reply('Замовлення видалено');

        $this->stat('Remove order', 'clearOrder', $bot);
    }

}