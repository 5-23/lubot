<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 10/13/2017
 * Time: 12:26 AM
 */

namespace App\Http\Controllers;

use BotMan\BotMan\BotMan;

class CatalogController extends Controller
{

    public function catalog(BotMan $bot)
    {
        $this->strategy($bot)->catalog();
        $bot->reply('Примітка. Для виклика Вашої корзини, напишіть - "Корзина"');

        $this->stat('Get catalog of products', 'catalog', $bot);
    }

    public function plastimake(BotMan $bot){
        $this->strategy($bot)->products('Plastimake');

        $this->stat('Get plastimake catalog', 'plastimake', $bot);
    }

    public function bundles(BotMan $bot){
        $this->strategy($bot)->products('Набори');

        $this->stat('Get bundles catalog', 'bundles', $bot);
    }

    public function accessories(BotMan $bot){
        $this->strategy($bot)->products('Аксесуари');

        $this->stat('Get accessories catalog', 'accessories', $bot);
    }

    public function dyes(BotMan $bot){
        $this->strategy($bot)->products('Барвники');

        $this->stat('Get dyes catalog', 'dyes', $bot);
    }

    public function show(BotMan $bot) {
        $itemId = preg_split('/\s+/', $bot->getMessage()->getText())[1];
        $this->strategy($bot)->show($itemId);

        $this->stat('Show one product, id='.$itemId, 'show', $bot);
    }

}