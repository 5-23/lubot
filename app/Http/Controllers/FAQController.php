<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 10/13/2017
 * Time: 10:31 PM
 */

namespace App\Http\Controllers;


use BotMan\BotMan\BotMan;

class FAQController extends Controller
{
    public function faq(BotMan $bot)
    {
        $this->strategy($bot)->faq();

        $this->stat('Call faq menu', 'faq', $bot);
    }

    public function whatIsPlastimake(BotMan $bot) {
        $this->strategy($bot)->about();

        $this->stat('Get about info', 'whatIsPlastimake', $bot);
    }

    public function deliveryAndPayment(BotMan $bot) {
        $bot->reply('Ви можете купити термопластик пластімейк з доставкою по Києву та Україні. '.
            'Безкоштовна кур\'єрська доставка по Києву або Новою Поштою по Україні при замовленні на '.
            'суму від 600 грн. Також ми доставляємо пластімейк кур\'єром по Києву - вартість '.
            '35 грн, оплата готівкою при отриманні та відправляємо пластімейк Новою Поштою по всій Україні,'.
            ' оплата на картку Приват Банку, або ж накладеним платижем при отриманні.');

        $this->stat('Get delivery and payment info', 'deliveryAndPayment', $bot);
    }

    public function whatIsThePrice(BotMan $bot) {
        $bot->reply('Перейдемо в каталог для ознайомлення з цінами');
        $bot->typesAndWaits(1);
        (new CatalogController())->catalog($bot);

        $this->stat('Check prices', 'whatIsThePrice', $bot);
    }

}