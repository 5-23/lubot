<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 10/13/2017
 * Time: 10:54 PM
 */

namespace App\Http\Controllers;


use BotMan\BotMan\BotMan;

class WelcomeController extends Controller
{
    public function welcome(BotMan $bot)
    {
        $user = $bot->getUser();
        $firstname = $user->getFirstName();

        $bot->reply('Вітаю'.($firstname ? ' '.$firstname : '').'! Мене звати Лу. Я робот. Зі мною, отримувати оперативну інформацію, переглядати каталоги, відслідковувати та робити замовлення стало набагато простіше!'
            .'. Якщо ж Ви хочете говорити з живою людиною, можете залишити довільне повідомлення в чаті, і воно'
            .' обов\'язково буде опрацьоване оператором.');
        $this->strategy($bot)->menu();

        $this->stat('Get welcome menu', 'welcome', $bot);
    }

    public function greeting(BotMan $bot) {
        $bot->reply('Привіт, людино!');
        $this->strategy($bot)->menu();
    }

    public function greetingSpec(BotMan $bot) {
        $user = $bot->getUser();
        $firstname = $user->getFirstName();

        $bot->reply('Привіт'.($firstname ? ', '.$firstname : '').'!');
    }

    public function menu(BotMan $bot) {
        $this->strategy($bot)->menu();
    }

}