<?php
use \App\Http\Controllers\CatalogController;
use \App\Http\Controllers\CartController;
use \App\Http\Controllers\FAQController;
use \App\Http\Controllers\WelcomeController;
use \App\Http\Controllers\CheckoutController;
use \App\Http\Controllers\TrackController;
use BotMan\BotMan\BotMan;

/** @var BotMan $botman */
$botman = resolve('botman');

$botman->hears('(П|п)р(и|і)в(і|е)т', WelcomeController::class.'@greeting');
$botman->hears('(П|п)р(и|і)в(і|е)т (Л|л)у', WelcomeController::class.'@greetingSpec');
$botman->hears('(С|с)тарт', WelcomeController::class.'@welcome');
$botman->hears('(П|п)очат(и|ок)', WelcomeController::class.'@welcome');
$botman->hears('(М|м)еню', WelcomeController::class.'@menu');

$botman->hears('faq', FAQController::class.'@faq');
$botman->hears('(Ч|ч)асті питання', FAQController::class.'@faq');
$botman->hears('(Щ|щ)о таке ((P|p)lastimake|(П|п)ласт(и|і)мейк)\??', FAQController::class.'@whatIsPlastimake');
$botman->hears('(Д|д)оставка та оплата', FAQController::class.'@deliveryAndPayment');
$botman->hears('(О|о)плата', FAQController::class.'@deliveryAndPayment');
$botman->hears('(Д|д)оставка', FAQController::class.'@deliveryAndPayment');
$botman->hears('(С|с)кільки коштує\??', FAQController::class.'@whatIsThePrice');
$botman->hears('(Ц|ц)ін(а|и)\??', FAQController::class.'@whatIsThePrice');
$botman->hears('(В|в)артість\??', FAQController::class.'@whatIsThePrice');

$botman->hears('(В|в)ідслідкувати', TrackController::class.'@track');

$botman->hears('(К|к)орзина', CartController::class.'@cart');
$botman->hears('(К|к)ошик', CartController::class.'@cart');
$botman->hears('(О|о)чистити корзину', CartController::class.'@clearCart');
$botman->hears('(О|о)чистити', CartController::class.'@clearCart');
$botman->hears('(В|в)идалити замовлення', CartController::class.'@clearOrder');

$botman->hears('(К|к)аталог', CatalogController::class.'@catalog');
$botman->hears('(Т|т)овари', CatalogController::class.'@catalog');
$botman->hears('(П|п)родукти', CatalogController::class.'@catalog');
$botman->hears('((P|p)lastimake|(П|п)ласт(и|і)мейк)', CatalogController::class.'@plastimake');
$botman->hears('(Н|н)абори', CatalogController::class.'@bundles');
$botman->hears('(А|а)ксесуари', CatalogController::class.'@accessories');
$botman->hears('(Б|б)арвники', CatalogController::class.'@dyes');
$botman->hears('(П|п)ігменти', CatalogController::class.'@dyes');

$botman->hears('(Ч|ч)ек', CheckoutController::class.'@cheque');
$botman->hears('(О|о)формити', CheckoutController::class.'@checkout');
$botman->hears('(О|о)формити замовлення', CheckoutController::class.'@checkout');


if (!\App\OrderItem::$CATALOG) {
    \App\OrderItem::initCatalog();
}
init($botman);

function init($bot) {
        foreach(\App\OrderItem::$CATALOG as $menu) {
            foreach($menu['items'] as $item) {
                $bot->hears($item->id, CartController::class.'@addToCart');
                $bot->hears('(В|в)идалити '.$item->id, CartController::class.'@removeFromCart');
                $bot->hears('(П|п)оказати '.$item->id, CatalogController::class.'@show');
            }
        }
}

$botman->fallback(function(BotMan $bot) {
    $bot->reply('Я Вас не розумію, напишіть "Меню" для виклику головного меню або зачекайте відповіді від оператора :)');
});


$dialogflow = \BotMan\BotMan\Middleware\ApiAi::create('b71dd842a2eb429d8a83793ad3d2fe6a')->listenForAction();
$botman->middleware->received($dialogflow);
$botman->hears('.*', function (BotMan $bot) {
    $extras = $bot->getMessage()->getExtras();
    $apiReply = $extras['apiReply'];
    $apiAction = $extras['apiAction'];
    $apiIntent = $extras['apiIntent'];
    \Illuminate\Support\Facades\Log::info('extras: '.json_encode($extras));
    $bot->reply($apiReply);
})->middleware($dialogflow);